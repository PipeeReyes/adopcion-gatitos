from django.shortcuts import render, HttpResponseRedirect, redirect
from django.template import RequestContext
from .models import Usuario, Mascota
from .forms import RegForm, MascForm
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import TemplateView

def formulario(request):
    return render(request, 'AdopcionDeGatos/Main.html')

@login_required(login_url="/accounts/login/")
def reg_new(request):
    
    if request.method == "POST":
        
        form = RegForm(request.POST)
        if form.is_valid():
            form.save()
            '''Usuarios = Usuario.objects.all()
            return render(request, 'AdopcionDeGatos/list.html', {'lista': Usuarios})'''
            return redirect('servicio')
    else:
        form = RegForm()
        return render(request, 'AdopcionDeGatos/formulario.html', {'form': form})

@login_required(login_url="/accounts/login/")
def listar(request):
    Usuarios = Usuario.objects.all().orderby('nombre')
    return render(request, 'AdopcionDeGatos/list.html', {'lista': Usuarios})

@login_required(login_url="/accounts/login/")
def reg_masc(request):
    if request.method == "POST":
        form = MascForm(request.POST, request.FILES)
        print(form.errors)
        if form.is_valid():
            form.save()
            mascotas = Mascota.objects.all()
            return render(request, 'AdopcionDeGatos/manlist.html', {'listam': mascotas})
    else:
        form = MascForm()
        return render(request, 'AdopcionDeGatos/mantenedor.html', {'form': form})
    
@login_required(login_url="/accounts/login/")
def masclist(request):
    mascotas = Mascota.objects.all()
    return render(request, 'AdopcionDeGatos/manlist.html', {'listam': mascotas})

@login_required(login_url="/accounts/login/")
def update_mascota(request, id):
    mascota = Mascota.objects.get(id=id)
    form = MascForm(request.POST or None, instance=mascota)
    print(mascota.nombre)
    if form.is_valid():
        form.save()
        mascotas = Mascota.objects.all()
        return render(request, 'AdopcionDeGatos/manlist.html', {'listam': mascotas})
    
    return render(request, 'AdopcionDeGatos/mantenedor.html', {'mascota': mascota}) 

@login_required(login_url="/accounts/login/")
def delete_mascota(request, id):
    mascota = Mascota.objects.get(id=id)

    if request.method == 'POST':
        mascota.delete()
        mascotas = Mascota.objects.all()
        return render(request, 'AdopcionDeGatos/manlist.html', {'listam': mascotas})

    return render(request, 'AdopcionDeGatos/confirmacion.html', {'mascota': mascota})

@login_required(login_url="/accounts/login/")
def listar_resc(request):
    mascotas = Mascota.objects.all().filter(estado='Rescatado')
    return render(request, 'AdopcionDeGatos/manlist2.html', {'listam': mascotas})

@login_required(login_url="/accounts/login/")
def listar_adop(request):
    mascotas = Mascota.objects.all().filter(estado='Adoptado')
    return render(request, 'AdopcionDeGatos/manlist3.html', {'listam': mascotas})

@login_required(login_url="/accounts/login/")
def listar_disp(request):
    mascotas = Mascota.objects.all().filter(estado='Disponible')
    return render(request, 'AdopcionDeGatos/manlist4.html', {'listam': mascotas})

@login_required(login_url="/accounts/login/")
def servicio(request):
    return render(request, 'AdopcionDeGatos/servicio.html')
