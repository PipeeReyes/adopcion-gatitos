from django.conf.urls import include , url
from django.urls import path
from . import views
from django.contrib.auth.views import LoginView

urlpatterns = [
    path('', views.formulario, name='formulario'),
    path('Usuario/new', views.reg_new, name='reg_new'),
    path('Usuario/list',views.listar),
    path('Usuario/man', views.reg_masc, name='reg_masc'),
    path('Usuario/manlist', views.masclist, name='masclist'),
    path('update/<int:id>/', views.update_mascota, name="update_mascota"),
    path('delete/<int:id>/', views.delete_mascota, name="delete_mascota"),
    path('Usuario/manlist2', views.listar_resc, name='rescatados'),
    path('Usuario/manlist3', views.listar_adop, name='adoptados'),
    path('Usuario/manlist4', views.listar_disp, name='disponibles'),
    path('Usuario/servicio', views.servicio, name='servicio')
    
]